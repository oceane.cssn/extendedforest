# extendedForest R package


I am hosting here the original extendedForest R package (Stephen J. Smith, Nick Ellis, C. Roland Pitcher
February 10, 2011) in case one needs to use it as a dependency of their own R package.

Just use

```
remotes::install_gitlab("oceane.cssn/extendedforest")
```


The extendedForest vignette is here: https://gradientforest.r-forge.r-project.org/Conditional-importance.pdf


The article on conditional permutations is described in Strobl et al. 2008: https://bmcbioinformatics.biomedcentral.com/articles/10.1186/1471-2105-9-307
